<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Rap2hpoutre\FastExcel\FastExcel;


class UserController extends Controller
{
    protected $model;

    public function __construct(User $user)
    {
        $this->model = $user;
    }

    public function index()
    {
        $data = $this->model->orderBy('id', 'desc')->get();
        return $this->successResponse($data);
    }

    public function show(Request $request)
    {
        $data = $this->model->where('id', $request->user)->first();
        return $this->successResponse($data);
    }

    public function store(Request $request)
    {
        $data = json_decode($request->getContent());
        $data->password = bcrypt($data->password);
        $this->model->create((array)$data);
        return $this->successResponse();
    }

    public function update(Request $request)
    {
        $data = json_decode($request->getContent());
        $user = $this->model->where('id', $request->user)->first();
        if (isset($data->password)) {
            $data->password = bcrypt($data->password);
        }
        $user->update((array)$data);
        return $this->successResponse();
    }

    public function destroy(Request $request)
    {
        $this->model->where('id', $request->user)->delete();
        return $this->successResponse($this->model->get());
    }
}

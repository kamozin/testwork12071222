<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBuyer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buyer', function (Blueprint $table) {
            $table->id();
            $table->text('name');
            $table->string('inn');
            $table->string('kpp');
            $table->string('ogrn');
            $table->text('address');
            $table->string('zip_code');
            $table->text('bank');
            $table->text('bank_details');
            $table->text('bank_address');
            $table->string('bank_account');
            $table->string('corr_account');
            $table->text('account_us');
            $table->text('account_eu');
            $table->string('bic');
            $table->text('swift_code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buyer');
    }
}

import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'users',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/users/index.vue')
  },
  {
    path: '/users/create',
    name: 'usersCreate',
    component: () => import(/* webpackChunkName: "about" */ '../views/users/create.vue')
  },
  {
    path: '/users/:id',
    name: 'usersEdit',
    component: () => import(/* webpackChunkName: "about" */ '../views/users/edit.vue')
  },
  {
    path: '/sender',
    name: 'sender',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/sender/index.vue')
  },
  {
    path: '/sender/create',
    name: 'senderCreate',
    component: () => import(/* webpackChunkName: "about" */ '../views/sender/create.vue')
  },
  {
    path: '/sender/:id',
    name: 'senderEdit',
    component: () => import(/* webpackChunkName: "about" */ '../views/sender/edit.vue')
  },
  {
    path: '/buyer',
    name: 'buyer',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/buyers/index.vue')
  },
  {
    path: '/buyer/create',
    name: 'buyerCreate',
    component: () => import(/* webpackChunkName: "about" */ '../views/buyers/create.vue')
  },
  {
    path: '/buyer/:id',
    name: 'buyerEdit',
    component: () => import(/* webpackChunkName: "about" */ '../views/buyers/edit.vue')
  },
  {
    path: '/package',
    name: 'package',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/package/index.vue')
  },
  {
    path: '/package/create',
    name: 'packageCreate',
    component: () => import(/* webpackChunkName: "about" */ '../views/package/create.vue')
  },
  {
    path: '/package/:id',
    name: 'packageEdit',
    component: () => import(/* webpackChunkName: "about" */ '../views/package/edit.vue')
  },
  {
    path: '/products',
    name: 'products',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/products/index.vue')
  },
  {
    path: '/products/create',
    name: 'productsCreate',
    component: () => import(/* webpackChunkName: "about" */ '../views/products/create.vue')
  },
  {
    path: '/products/:id',
    name: 'productsEdit',
    component: () => import(/* webpackChunkName: "about" */ '../views/products/edit.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
